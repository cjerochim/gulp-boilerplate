# README #

### Gulp Boilerplate    
Provides you with the basic tools allow you to get up and running fast, for standalone projects.

### How do I get set up? ###

#####Requirements

**Need to be installed globally.**

- Node - [download](https://nodejs.org/)
- Gulp - [Instructions](http://gulpjs.com/)
- Bower - [Instructions](http://bower.io/)

Checkout project and run the below commands in terminal to get up and running.

Make sure you in the project directory 
``` cd <directory of project> ```

Install all Gulp dependencies 

``` npm install ``` 

you may need to run 

```sudo```

Install all Bower dependencies

``` bower install ```


All configurations are referenced in the 'config.js' file located in the gulp folder. 

####Gulp tasks

``` gulp ```

__ Will run local host, build and run watching on files specified in the 'config.js' located in the 'gulp' folder' __

``` gulp build ```


__ Will build the project this will include the following __

* Assemble - Handlebars (HTML)
* Browserify - (Javascript)
* SASS - (CSS)
* Copy - Copy all specified files e.g. images, fonts, and vendors - Note vendors have to be manually defined in the configuration 'config.js' in the 'gulp' folder.

```gulp clean```


__ Will delete the build. __

Enjoy.