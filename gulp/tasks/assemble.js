'use strict';

var gulp = require('gulp'),
    assemble = require('gulp-assemble'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    config = require('../config'),
    utils = require('../utils');


gulp.task('assemble', function() {
    return gulp.src(config.assemble.paths.src)
        .pipe(plumber({ errorHandler: utils.errorHandler }))
        .pipe(assemble(config.assemble.properties))
        .pipe(gulp.dest(config.assemble.paths.dest))
        .pipe(reload({stream: true}))
        .pipe(notify({
            title: 'Assemble Complete',
            message: 'Good Job!'
        }));
});
