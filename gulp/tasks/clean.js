'use strict';

var gulp = require('gulp'),
    clean = require('gulp-clean'),
    notify = require('gulp-notify'),
    config = require('../config');


gulp.task('clean', function() {
   return gulp.src(config.clean.input, { read: false })
       .pipe(clean())
       .pipe(notify({
           title: 'Clean Complete',
           message: 'Good Job'
       }))
});