'use strict';


var gulp 				= require('gulp');
var watch 				= require('gulp-watch');
var batch 				= require('gulp-batch');
var config 				= require('../config');


gulp.task('watch', function() {
	var collection = config.watch;
	collection.forEach(function(watchItem) {
			watch(watchItem.input, batch(function(events, done) {
				gulp.start(watchItem.task, done);
			}));
	});
});