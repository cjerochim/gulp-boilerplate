'use strict';

var gulp = require('gulp'),
    config = require('../config');



gulp.task('copy:fonts', function() {
    return gulp.src(config.copy.fonts.input)
        .pipe(gulp.dest(config.copy.fonts.output));
});


gulp.task('copy:images', function() {
    return gulp.src(config.copy.images.input)
        .pipe(gulp.dest(config.copy.images.output));
});

gulp.task('copy:vendors', function() {
    return gulp.src(config.copy.vendors.input)
        .pipe(gulp.dest(config.copy.vendors.output));
});


gulp.task('copy', ['copy:images','copy:fonts', 'copy:vendors']);