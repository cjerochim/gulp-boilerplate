'use strict';

var gulp 					= require('gulp');
var postcss 				= require('gulp-postcss');
var pxtoem 					= require('postcss-px-to-em');
var plumber 				= require('gulp-plumber');
var notify 					= require('gulp-notify');
var autoprefixer 			= require('autoprefixer');
var rucksack 				= require('rucksack-css');
var customMedia 			= require('postcss-custom-media');
var precss 					= require('precss');
var lost 					= require('lost');
var config 					= require('../config');
var utils 					= require('../utils');


gulp.task('css', function() {
	var processors = [
	autoprefixer({ browsers: ['last 1 version']}),
	precss,
	rucksack,
	pxtoem,
	customMedia,
	lost
	];

	return gulp.src(config.css.input)
		.pipe(plumber({ errorHandler: utils.errorHandler }))
		.pipe(postcss(processors))
		.pipe(gulp.dest(config.css.output))
		.pipe(notify({
			title: 'PostCSS Complete'
		}));
});