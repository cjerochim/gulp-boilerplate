'use strict';


var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    config = require('../config'),
    utils = require('../utils');


gulp.task('browserify', function() {
    return gulp.src(config.browserify.input)
        .pipe(plumber({ errorHandler: utils.errorHandler }))
        .pipe(browserify())
        .pipe(gulp.dest(config.browserify.output))
        .pipe(reload({ stream: true }))
        .pipe(notify({
            title: 'Browserify Complete',
            message:'Good Job!'
        }));
});