var notify = require('gulp-notify');

module.exports = {
    errorHandler: function(err) {
        notify.onError({
            message: 'Error: <%= error.message %>'
        })(err);
        this.emit('end');
    }
};