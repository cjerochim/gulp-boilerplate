'use strict';


module.exports.css = {
    input: './src/css/screen.css',
    output: './public/css/'
};

module.exports.assemble = {
    paths: {
        src: './src/templates/pages/**/*.hbs',
        dest: './public/'
    },
    properties: {
        data: 'src/data/*.json',
        partials: 'src/templates/partials/**/*.hbs',
        layoutdir: 'src/templates/layouts/',
        helpers: 'src/helpers/**/*.js'
    }
};



module.exports.copy = {
    images: {
        input: './src/images/**/*.{jpg,png,gif}',
        output: './public/images/'
    },
    fonts: {
        input: './src/fonts/**/*.*',
        output: './public/fonts/'
    },
    vendors: {
        input: [],
        output: './public/vendors/'
    }
};



module.exports.browserify = {
    input: './src/js/main.js',
    output: './public/js/'
};


module.exports.browserSync = {
    input: './public'
};


module.exports.clean = {
    input: './public'
};


module.exports.watch = [
    { input: './src/css/*.css', task: ['css'] },
    { input: './src/templates/**/*.hbs', task: ['assemble'] },
    { input: './src/js/**/*.js', task: ['browserify'] }
];